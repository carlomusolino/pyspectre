#--------------------------------------
# orthogonal bases for interpolation
#--------------------------------------


#----------------------------------------------------------------------------------------
#
# The polynomial classes are intended to provide an easy interface to hold:
# 1. Collocation Points;
# 2. Weights;
# 3. Polynomial values of a given order (method);
# 4. Easy operations on the given basis (derivatives, multiplication by x etc..) (methods);
# 5. Function interpolation and decomposition (methods).
#
#----------------------------------------------------------------------------------------
#
#
# For an order N computation, the calling function should create N instances of
# the relevant polynomial class, and each will handle its own operations.
# Operations that are basis independent such as projection will be handled by
# separate functions.
#
#
#------------------------------------------------------------------------------------------

import numpy as np
#from mappings import *


class Chebyshev:
    name = "Collocation points and methods for Chebyshev base decomposition"
    descr = "Defined on [-1,1] spectral convergence on any $C^\infty$ function"

    def __init__(self, N=1, node_t="Gauss-Lobatto"):
        assert( type(N) == int )
        # Order
        self.ord = N
        # Type of nodes (Gauss, Gauss-Radau, Gauss-Lobatto)
        self.node_type = node_t
        # Linear operators for the multiplication/division by x
        [self.timesX, self.overX] = self.timesOverx()
        # Linear operators for first and second derivatives
        self.D = self.diff()
        self.DD = self.secDiff()
        #self.p = []
        # Nodes and weights 
        self.nodes = []
        self.weights = []
        if N == 0 :
            self.nodes = []
            self.weights = []
        elif node_t == "Gauss" :
            self.nodes = np.cos( (2*np.arange(N+1)+1)*np.pi / (2*N +2))
            self.weights = np.ones(np.shape(self.nodes))*np.pi/(N+1)
        elif node_t == "Gauss-Radau":
            self.nodes = np.cos( 2*np.pi*np.arange(N+1)/(2*N+1) )
            self.weights = np.ones(np.shape(self.nodes))*2*np.pi/(2*N+1)
            self.weights[0] /= 2
        else :
            self.nodes = - np.cos( np.arange(N+1)*np.pi / N )
            self.weights = np.ones(N+1) * np.pi/N
            self.weights[0] /= 2
            self.weights[N] /= 2
        return
            
    def eval(self,x=None):
        if x is None: x = self.nodes
        #self.p = chebyshev(x,n)
        return chebyshev(x,self.ord)
    
    # following methods are utilities that act on already decomposed functions

    def timesOverx(self):
        '''
        return the matrix corresponding with the linear operator X (multiply by x)
        '''
        #if self.ord == 0: return
        #elif self.ord == 1:
        #    return 0.5*( 2*f_tilde[0] + f_tilde[2] )
        #else:
        #    return 0.5*( 2*f_tilde[self.ord-1] + f_tilde[self.ord+1])
        N = self.ord
        if N == 0: N = 1
        X = np.zeros([N+1,N+1])
        for i in np.arange(1,N+1):
            for j in np.arange(0,N+1):
                if j == i+1 or j == i-1: X[i,j] = 1
        #X[N,N-1] = 1
        X[0,1] = 1; X[1,0] = 2
        X = 0.5*X
        return X, np.eye(N) 
        
    def diff(self):
        #if self.ord == 0: d = 0
        #else: d = 1
        #b_n = 0
        #for p in np.arange(self.ord+1,N):
        #    if (self.ord+p)%2 == 1:
        #        b_n +=  p*f_tilde[p]
        #    pass
        #b_n *= 2/(d+1)
        #return b_n
        N = self.ord
        if N == 0: N = 1
        D = np.zeros([N+1,N+1])
        for i in range(N+1):
            for j in np.arange(i+1,N+1):
                if (i+j)%2 == 1:
                    D[i,j] = 2.0*j
        D[0,:] = D[0,:]*0.5
        return D
    
    def secDiff(self):
        #d = 0
        #if self.ord == 0 : d=1
        #b_n = 0
        #for p in np.arange(self.ord+2,N):
        #    if (self.ord+p)%2 == 0:
        #        b_n += p*(p**2-self.ord**2)*f_tilde[p]
        #    pass
        #b_n /= (d+1)
        #return b_n
        N = self.ord
        if N == 0: N = 1
        DD = np.zeros([N+1,N+1])
        for i in range(N+1):
            for j in np.arange(i+2,N+1):
                if (i+j)%2==0:
                    DD[i,j] = float(j*(j**2-i**2))
        DD[0,:] = 0.5*DD[0,:]
        return DD

    def decompose(self,func):
        '''
        Decompose a given function on the chosen basis of orthonormal polynomials to a certain order N using Gaussian, Gauss-Radau, or Gauss-Lobatto quadrature
        '''
        N = self.ord
        f_tilde = np.zeros(N+1)
        x = self.nodes
        w = self.weights 
        for i in range(N+1):
            p_vals = chebyshev(x,i)
            gamma = np.sum(p_vals**2*w)
            f_tilde[i] = np.sum(func(x)*p_vals*w)/gamma
        return f_tilde
    
    def interpolate(self,func,x):
        '''
        Interpolate func to the order n on the chosen basis of orthonormal polynomials using Gauss, Gauss-Radau, Gauss-Lobatto quadrature: we compute
        I_N f(x) = sum_{n=1} ^N f_tilde[n] * T_n (x)
        where T_n is the nth order polynomial in the basis and f_tilde the quadrature decomposition of f.
        '''
        N = self.ord
        f_t = self.decompose(func)
        f = np.zeros(np.shape(x))
        for i in range(N+1):
            f += f_t[i]*chebyshev(x,i)
        return f

class Legendre:
    pass

    
#--------------------------------------
# END OF CLASSES
#--------------------------------------

def chebyshev(x,n):
    '''
Computes the n-th Chebyshev polynomial at x. x can be an array.
    '''
    ## will set a dictionary containing the first few for efficiency !
    if n == 0:
        return np.ones(np.shape(x))
    elif n == 1:
        return x
    else:
        return 2*x*chebyshev(x,n-1)-chebyshev(x,n-2)


    
