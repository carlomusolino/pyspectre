#------------------------------------------------------------------------------
# Define classes implementing affine mappings to translate arbitrary intervals
# on which functions are defined to the domain of definition of the relevant
# spectral basis.
#------------------------------------------------------------------------------

import numpy as np 

class map_af:
    '''
Simple affine map that takes [-1, 1] into [a,b] for use with Chebyshev polynomial basis:
    \xi = \alpha x + \beta
    '''
    def __init__(self,bounds,n=100,type="Rare"):     
        if type == "Rare":
            self.alpha = bounds[1] - bounds[0]
            self.beta = bounds[0]
        elif type == "Compact":
            # Affine mapping for compactified domain extending to $\infty$ not supported yet 
            pass
        self.x = np.linspace(-1,1,n)
        self.xi = self.alpha * self.x + self.beta
        
