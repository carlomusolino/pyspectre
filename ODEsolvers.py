#----------------------------------
#
# Single domain linear ODEs spectral solvers
#
#-----------------------------------

class BoundaryConditions:

    def __init__(self,vals,type="von-Neumann"):
        self.type = type
        self.values = vals

    def coordChange(self,old_int,new_int,jac):
        '''
        Transform boundary conditions in case of coordinate change
        '''
        pass

class tauSolver:
    
    
#-----------------------------------
#
# Problem of the form Lu(x) = s(x)
# L 2nd order linear differential operator
#
#-----------------------------------

def tausolve(L,s,bounds,N=10):

    c = Chebyshev(N=N)
    s_t = c.decompose(s)
    s_t[N-1] = bounds[0]; s_t[N] = bounds[1]
    for i in range(N+1):
        L[N-1,i] = chebyshev(-1,i)
        L[N,i] = chebyshev(1,i)
    u_t = np.linalg.solve(L,s)
    return u_t
    
